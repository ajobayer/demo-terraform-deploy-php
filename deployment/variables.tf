variable "environment_type"       {
  description = ""
  default     = "dev"
}

# Instance related varibales are defined here
variable "ami_id"                 {
  description = ""
  default     = "ami-1c47407f"
}
variable ssh_pem_key              {
  description = ""
  default     = "DemoWebAppDeploy"
}
variable "instance_type"          {
  description = ""
  default     = "t2.small"
}

# DataBase related varibales are defined here
variable database_name            {
  description = ""
  default     = "demomysql"
} 
variable database_username        {
  description = ""
  default     = "root1234"
}
variable dataabse_password        {
  description = ""
  default     = "root1234"
}
variable database_instance_type   {
  description = ""
  default     = "db.t2.micro"
}
# DNS related variables are defined here
variable param_webaliasname       {
  description = ""
  default     = "webalias"
}
variable param_hostedzoneid       {
  description = ""
  default     = "Z1T2MJU9AGGKMW"
}
variable param_webdnsname         {
  description = ""
  default     = "web"
}
variable param_hostedzonedomain   {
  description = ""
  default     = "ajobayer.name"
}