# Environment values: dev, stage, prod
# environment_type            = "dev"
# ami_id                      = "ami-1c47407f"
# instance_type               = "t2.small"
# ssh_pem_key                 = "DemoWebAppDeploy"

# database_name               = "demomysql"
# database_username           = "root1234"
# dataabse_password           = "root1234"
# database_instance_type      = "db.t2.micro"

# param_webaliasname          = "webalias"
# param_hostedzoneid          = "Z1T2MJU9AGGKMW"
# param_webdnsname            = "web"
# param_hostedzonedomain      = "ajobayer.name"

# variable "environment_type"       {}
# variable "ami_id"                 {}
# variable ssh_pem_key              {}
# variable "instance_type"          {}
# variable database_name            {} 
# variable database_username        {}
# variable dataabse_password        {}
# variable database_instance_type   {}
# variable param_webaliasname       {}
# variable param_hostedzoneid       {}
# variable param_webdnsname         {}
# variable param_hostedzonedomain   {}