
# App Server ALB DNS URL
output "app_server_dns_name"   {
   value = ["http://${aws_cloudformation_stack.DemoAppALB.outputs["APPLoadBalancerUrl"]}"]
}
# Web server DNS name
output "web_server_dns_name"   {
    value = "http://${aws_cloudformation_stack.DemoDNS.outputs["WEBRoute53"]}"
}

