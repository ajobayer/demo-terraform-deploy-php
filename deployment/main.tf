# aws configure --profile ajobayer
provider "aws" {
  region                  = "ap-southeast-2"
  shared_credentials_file = "$HOME/.aws/credentials"
  profile                 = "ajobayer"
}

resource "aws_cloudformation_stack" "DemoIAMRole" {
  name = "DemoIAMRole"

  parameters = {
    PMServerEnv = "${var.environment_type}"
  }

  capabilities  = ["CAPABILITY_IAM"]
  template_body = "${file("${path.module}/infrastructure/webapp-iam.yaml")}"
}

resource "aws_cloudformation_stack" "DemoS3Bucket" {
  name = "DemoS3Bucket"

  parameters = {
    PMServerEnv      = "${var.environment_type}"
    PMRegionAStorage = "Glacier"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-s3bucket.yaml")}"
}

resource "aws_cloudformation_stack" "DemoVPC" {
  name = "DemoVPC"

  parameters = {
    PMServerEnv          = "${var.environment_type}"
    PMVpcCIDR            = "10.0.0.0/16"
    PMPublicSubnet1CIDR  = "10.0.1.0/24"
    PMPublicSubnet2CIDR  = "10.0.2.0/24"
    PMPrivateSubnet1CIDR = "10.0.3.0/24"
    PMPrivateSubnet2CIDR = "10.0.4.0/24"
    PMFlowLogRole        = "${aws_cloudformation_stack.DemoIAMRole.outputs["VPCFlowLogRoleArn"]}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-vpc.yaml")}"
}

resource "aws_cloudformation_stack" "DemoSecurityGroup" {
  name = "DemoSecurityGroup"

  parameters = {
    PMServerEnv = "${var.environment_type}"
    PMOWNIP     = "0.0.0.0/0"
    PMVPC       = "${aws_cloudformation_stack.DemoVPC.outputs["VPC"]}"
    PMNACL      = "${aws_cloudformation_stack.DemoVPC.outputs["DemoNetworkACL"]}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-securitygroup.yaml")}"
}

resource "aws_cloudformation_stack" "DemoBastionHost" {
  name = "DemoBastionHost"

  parameters = {
    PMServerEnv          = "${var.environment_type}"
    PMKeyName            = "${var.ssh_pem_key}"
    PMInstanceType       = "${var.instance_type}"
    PMRegionAMI          = "${var.ami_id}"
    PMProxyBastionHostSG = "${aws_cloudformation_stack.DemoSecurityGroup.outputs["ProxyBastionHostSG"]}"

    # PMBastionELB: !GetAtt "DemoAppALB.Outputs.APPLoadBalancer"
    PMIAMS3CWInstanceProfile = "${aws_cloudformation_stack.DemoIAMRole.outputs["IAMS3CWInstanceProfile"]}"
    PMPublicSubnets          = "${aws_cloudformation_stack.DemoVPC.outputs["PublicSubnets"]}"
    PMASMIN                  = 2
    PMASMAX                  = 2
    PMASDES                  = 2
  }

  template_body = "${file("${path.module}/infrastructure/webapp-bastion-host.yaml")}"
}

resource "aws_cloudformation_stack" "DemoRDS" {
  name       = "DemoRDS"
  depends_on = ["aws_cloudformation_stack.DemoSecurityGroup"]

  parameters = {
    PMServerEnv           = "${var.environment_type}"
    DatabaseName          = "${var.database_name}"
    DatabaseUser          = "${var.database_username}"
    DatabasePassword      = "${var.dataabse_password}"
    DatabaseSize          = "5"
    DatabaseEngine        = "mysql"
    DatabaseInstanceClass = "${var.database_instance_type}"
    PMRDSSG               = "${aws_cloudformation_stack.DemoSecurityGroup.outputs["RDSSG"]}"
    PMPrivateSubnets      = "${aws_cloudformation_stack.DemoVPC.outputs["PrivateSubnets"]}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-rds.yaml")}"
}

resource "aws_cloudformation_stack" "DemoAppALB" {
  name = "DemoAppALB"

  parameters = {
    PMServerEnv = "${var.environment_type}"
    PMVPC       = "${aws_cloudformation_stack.DemoVPC.outputs["VPC"]}"
    PMAPPALBSG  = "${aws_cloudformation_stack.DemoSecurityGroup.outputs["WebAppALBSG"]}"

    # PMS3Logging     = "${aws_cloudformation_stack.DemoS3Bucket.outputs["S3Logging"]}"
    PMPublicSubnets = "${aws_cloudformation_stack.DemoVPC.outputs["PublicSubnets"]}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-alb-appserver.yaml")}"
}

resource "aws_cloudformation_stack" "DemoAppAutoScaling" {
  name       = "DemoAppAutoScaling"
  depends_on = ["aws_cloudformation_stack.DemoAppALB"]

  parameters = {
    PMServerEnv              = "${var.environment_type}"
    PMKeyName                = "${var.ssh_pem_key}"
    PMInstanceType           = "${var.instance_type}"
    PMRegionAMI              = "${var.ami_id}"
    PMAPPHostSG              = "${aws_cloudformation_stack.DemoSecurityGroup.outputs["WebAppHostSG"]}"
    PMAPPLoadBalancer        = "${aws_cloudformation_stack.DemoAppALB.outputs["APPLoadBalancer"]}"
    PMIAMS3CWInstanceProfile = "${aws_cloudformation_stack.DemoIAMRole.outputs["IAMS3CWInstanceProfile"]}"
    PMPrivateSubnets         = "${aws_cloudformation_stack.DemoVPC.outputs["PrivateSubnets"]}"
    PMASMIN                  = 1
    PMASMAX                  = 1
    PMASDES                  = 1

    #PMWEBDOMAIN: !FindInMap ["EnvMap", !Ref "AWS::StackName", "WEBDOMAIN"]
    PMDataBaseEndpoint = "${aws_cloudformation_stack.DemoRDS.outputs["RdsDbEndpoint"]}"
    PMDataBaseName     = "${aws_cloudformation_stack.DemoRDS.outputs["DbName"]}"
    PMDataBaseUserName = "${aws_cloudformation_stack.DemoRDS.outputs["DbUser"]}"
    PMDataBasePassword = "${aws_cloudformation_stack.DemoRDS.outputs["DbPassword"]}"
    pTargetGroup       = "${aws_cloudformation_stack.DemoAppALB.outputs["rTargetGroup"]}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-asg-appserver.yaml")}"
}

resource "aws_cloudformation_stack" "DemoWebALB" {
  name       = "DemoWebALB"
  depends_on = ["aws_cloudformation_stack.DemoAppAutoScaling"]

  parameters = {
    PMServerEnv = "${var.environment_type}"
    PMVPC       = "${aws_cloudformation_stack.DemoVPC.outputs["VPC"]}"

    # PMWEBALBSG      = "${aws_cloudformation_stack.DemoSecurityGroup.outputs["WebAppALBSG"]}"
    PMAPPALBSG = "${aws_cloudformation_stack.DemoSecurityGroup.outputs["WebAppALBSG"]}"

    # PMS3Backup      = "${aws_cloudformation_stack.DemoS3Bucket.outputs["S3Backup"]}"
    # PMS3Logging     = "${aws_cloudformation_stack.DemoS3Bucket.outputs["S3Logging"]}"
    PMPublicSubnets = "${aws_cloudformation_stack.DemoVPC.outputs["PublicSubnets"]}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-alb-webserver.yaml")}"
}

resource "aws_cloudformation_stack" "DemoWebAutoScaling" {
  name       = "DemoWebAutoScaling"
  depends_on = ["aws_cloudformation_stack.DemoWebALB"]

  parameters = {
    PMServerEnv              = "${var.environment_type}"
    PMKeyName                = "${var.ssh_pem_key}"
    PMInstanceType           = "${var.instance_type}"
    PMRegionAMI              = "${var.ami_id}"
    PMProxyHostSG            = "${aws_cloudformation_stack.DemoSecurityGroup.outputs["WebAppHostSG"]}"
    PMWEBLoadBalancer        = "${aws_cloudformation_stack.DemoWebALB.outputs["WEBLoadBalancer"]}"
    PMIAMS3CWInstanceProfile = "${aws_cloudformation_stack.DemoIAMRole.outputs["IAMS3CWInstanceProfile"]}"
    PMPrivateSubnets         = "${aws_cloudformation_stack.DemoVPC.outputs["PrivateSubnets"]}"
    PMAPPLoadBalancerUrl     = "${aws_cloudformation_stack.DemoAppALB.outputs["APPLoadBalancerUrl"]}"
    PMASMIN                  = 1
    PMASMAX                  = 1
    PMASDES                  = 1

    #PMWEBDOMAIN: !FindInMap ["EnvMap", !Ref "AWS::StackName", "WEBDOMAIN"]
    pTargetGroup = "${aws_cloudformation_stack.DemoWebALB.outputs["rTargetGroup"]}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-asg-webserver.yaml")}"
}

resource "aws_cloudformation_stack" "DemoDNS" {
  name       = "DemoDNS"
  depends_on = ["aws_cloudformation_stack.DemoWebALB", "aws_cloudformation_stack.DemoWebAutoScaling"]

  parameters = {
    ParamWebAlbDnsName       = "${aws_cloudformation_stack.DemoWebALB.outputs["WEBLBDNSName"]}"
    ParamaWebAlbHostedZoneId = "${aws_cloudformation_stack.DemoWebALB.outputs["WEBLBHostedZoneId"]}"
    ParamWebAliasName        = "${var.param_webaliasname}"
    ParamWebDnsName          = "${var.param_webdnsname}"
    ParamHostedZoneId        = "${var.param_hostedzoneid}"
    ParamHostedZoneDomain    = "${var.param_hostedzonedomain}"
  }

  template_body = "${file("${path.module}/infrastructure/webapp-route53.yaml")}"
}

# DemoCloudWatch:
#   Type: "AWS::CloudFormation::Stack"
#   DependsOn:
#   - "DemoWebAutoScaling"
#   Properties:
#     TemplateURL: !Sub "${PMTemplateURL}/webapp-cloudwatch.yaml"
#     TimeoutInMinutes: '5'
#     Parameters:
#       PMWebScalingGroup: !GetAtt "DemoWebAutoScaling.Outputs.WebScalingGroup"
#       PMWebServerScaleUpPolicy: !GetAtt "DemoWebAutoScaling.Outputs.WebServerScaleUpPolicy"
#       PMWebServerScaleDownPolicy: !GetAtt "DemoWebAutoScaling.Outputs.WebServerScaleDownPolicy"
#       PMAppScalingGroup: !GetAtt "DemoAppAutoScaling.Outputs.AppScalingGroup"
#       PMAPPServerScaleUpPolicy: !GetAtt "DemoAppAutoScaling.Outputs.APPServerScaleUpPolicy"
#       PMAPPServerScaleDownPolicy: !GetAtt "DemoAppAutoScaling.Outputs.APPServerScaleDownPolicy"


# Extra Data Sources for Outputs
# data "template_file" "app_server_dns_value"{
#   vars {
#     first_val  = "http//"
#     second_val = "${aws_cloudformation_stack.DemoAppALB.outputs["APPLoadBalancerUrl"]}"
#   }
# }

