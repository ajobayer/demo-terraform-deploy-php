<?php session_start(); ?>
<html>
<head>
    <title>Homepage</title>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
 
<body>
    <div id="header">
        Welcome to my personal product inventory website!
    </div>
    <?php
    if(isset($_SESSION['valid'])) {            
        include("connection.php");                    
        $result = mysqli_query($mysqli, "SELECT * FROM products");
    ?>                
        Welcome <?php echo $_SESSION['name'] ?> ! If you are not admin you can only view and add product into inventory <a href='logout.php'>Logout</a><br/>
        <br/>
        <a href="index.php">Home</a> | <a href="add.html">Add New Data</a>

        <table width='80%' border=0>
        <tr bgcolor='#CCCCCC'>
            <td>Name</td>
            <td>Quantity</td>
            <td>Price (USD)</td>
        </tr>
        <?php
        while($res = mysqli_fetch_array($result)) {        
            echo "<tr>";
            echo "<td>".$res['name']."</td>";
            echo "<td>".$res['qty']."</td>";
            echo "<td>".$res['price']."</td>";      
        }
        ?>

        <br/><br/>
    <?php    
    } else {
        echo "You must be logged in to view this page.<br/><br/>";
        echo "<a href='login.php'>Login</a> | <a href='register.php'>Register</a>";
    }
    ?>
    <div id="footer">
        Created by <a href="http://github.com/ajobayer" title="Abdulalh Jobayer">Abdulalh Jobayer</a>
    </div>
</body>
</html>
